# [2.0.0](https://gitlab.com/to-be-continuous/gitleaks/compare/1.3.0...2.0.0) (2022-08-05)


### Features

* adaptive pipeline ([0b07845](https://gitlab.com/to-be-continuous/gitleaks/commit/0b07845e0db777c77bd448ced1dacf03f16cd996))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [1.3.0](https://gitlab.com/to-be-continuous/gitleaks/compare/1.2.5...1.3.0) (2022-05-01)


### Features

* configurable tracking image ([f6e71c9](https://gitlab.com/to-be-continuous/gitleaks/commit/f6e71c9e274588615a120aae08fdfdac6c251f02))

## [1.2.5](https://gitlab.com/to-be-continuous/gitleaks/compare/1.2.4...1.2.5) (2022-04-19)


### Bug Fixes

* add build dir in safe.directory ([3271704](https://gitlab.com/to-be-continuous/gitleaks/commit/3271704f698ee8ca91177394bfd7f255d8a1cee2))

## [1.2.4](https://gitlab.com/to-be-continuous/gitleaks/compare/1.2.3...1.2.4) (2021-12-07)


### Bug Fixes

* replace obsolete --config-path= args by --config in gitleaks rules ([577e0c8](https://gitlab.com/to-be-continuous/gitleaks/commit/577e0c8b595d4e5ffa939a745a4ae2f5e7d9c775))

## [1.2.3](https://gitlab.com/to-be-continuous/gitleaks/compare/1.2.2...1.2.3) (2021-12-02)


### Bug Fixes

* update command options to be compliant with latest Gitleaks version ([59d39b7](https://gitlab.com/to-be-continuous/gitleaks/commit/59d39b707376cd82803a83370a0961dca12a55f8))

## [1.2.2](https://gitlab.com/to-be-continuous/gitleaks/compare/1.2.1...1.2.2) (2021-10-07)


### Bug Fixes

* use master or main for production env ([af9620f](https://gitlab.com/to-be-continuous/gitleaks/commit/af9620fb9716383ddb5005d42a9bf64a9f4be194))

## [1.2.1](https://gitlab.com/to-be-continuous/gitleaks/compare/1.2.0...1.2.1) (2021-09-03)

### Bug Fixes

* Change boolean variable behaviour ([f0ad30e](https://gitlab.com/to-be-continuous/gitleaks/commit/f0ad30e5fd81cccaace51201d5d68a7979cd7ef3))

## [1.2.0](https://gitlab.com/to-be-continuous/gitleaks/compare/1.1.1...1.2.0) (2021-06-10)

### Features

* move group ([554f528](https://gitlab.com/to-be-continuous/gitleaks/commit/554f52851c99aefa8338b9ef59e6476bd35c4407))

## [1.1.1](https://gitlab.com/Orange-OpenSource/tbc/gitleaks/compare/1.1.0...1.1.1) (2021-06-04)

### Bug Fixes

* force clone full commits history in gitleaks complete analysis ([0b2c51e](https://gitlab.com/Orange-OpenSource/tbc/gitleaks/commit/0b2c51ef5d9cbb2794073939c2c18e9a457e5b66))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/gitleaks/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([9b701e5](https://gitlab.com/Orange-OpenSource/tbc/gitleaks/commit/9b701e5c8c88881aaba689696810978e43abe7ea))

## 1.0.0 (2021-05-06)

### Features

* initial release ([fe810ba](https://gitlab.com/Orange-OpenSource/tbc/gitleaks/commit/fe810ba058b4cac55d5bf4ccb342e6e5945d2045))
